const db = require('../lib/db');

exports.add = function(par, fn) {
	db.query('INSERT INTO topics SET ?', [par], fn);
}

exports.list = function(par, fn) {
	var clause =[], values =[], order =[], order_val =[],
		limit = par.limit,
		start = par.start;

	var query1 = "SELECT COUNT(*) jml FROM topics",
		query2 = "SELECT id,name,info,created_at FROM topics";
	if(clause.length > 0){
		query1+='WHERE '+clause.join(' AND ');
		query2+='WHERE '+clause.join(' AND ');
	}
	if(par.order)query2 += " ORDER BY "+par.order;
	else query2 += " ORDER BY id DESC";

	db.query(query1, values, (e, r) => {
		if(e) return console.log('list get total:',e,query1,values) || fn(e);

		if(r[0].jml == 0) return fn(null,{stat:1, total:0, msg:'Data not found'});

		db.query(query2+" LIMIT ?,?", values.concat([start, limit]), function(e, rsl){
			if(e) return console.log('list get data:',e,query2,values) || fn(e);
			fn(null,{stat:1, total:r[0].jml, data:rsl});
		});
	});
};

exports.detail = function(par, fn){
	db.query("SELECT * FROM topics WHERE id=?", par, fn);
}

exports.update = function(par, fn){
	db.query("UPDATE topics SET ? WHERE id=?", [par.update, par.id],fn);
}

exports.delete = function(par, fn){
	db.query("DELETE FROM topics WHERE id=?", par, fn);
}
