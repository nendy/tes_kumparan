const db = require('../lib/db');

exports.add = function(par, fn) {
	db.query('INSERT INTO news SET ?', [par], fn);
}

exports.list = function(par, fn) {
	var clause =[], values =[], order =[], order_val =[],
		limit = par.limit,
		start = par.start;

	var query1 = "SELECT COUNT(*) jml FROM news",
		query2 = "SELECT id,title,content,created_at FROM news";
	if(clause.length > 0){
		query1+='WHERE '+clause.join(' AND ');
		query2+='WHERE '+clause.join(' AND ');
	}
	if(par.order)query2 += " ORDER BY "+par.order;
	else query2 += " ORDER BY id DESC";

	db.query(query1, values, (e, r) => {
		if(e) return console.log('list get total:',e,query1,values) || fn(e);

		if(r[0].jml == 0) return fn(null,{stat:1, total:0, msg:'Data not found'});

		db.query(query2+" LIMIT ?,?", values.concat([start, limit]), function(e, rsl){
			if(e) return console.log('list get data:',e,query2,values) || fn(e);
			fn(null,{stat:1, total:r[0].jml, data:rsl});
		});
	});
};

exports.detail = function(par, fn){
	db.query("SELECT * FROM news WHERE id=?", par, fn);
}

const update_news_topic = (news, topics, cb) => {
	let arr = [];
	topics.forEach(t => {
		arr.push([news,t]);
	})
	console.log('updating:', arr);
	db.query('INSERT INTO news_topic (news_id,topic_id) VALUES ? ON DUPLICATE KEY UPDATE topic_id=topic_id', [arr], cb);
}

exports.update = function(par, fn){

	if(Object.keys(par.update).length > 0) {
		console.log('updating:', par.update);
		db.query("UPDATE news SET ? WHERE id=?", [par.update, par.id], (e, r) => {
			if(e) return fn(e);
			if(par.topics && par.topics.length > 0) update_news_topic(par.id, par.topics, fn);
			return fn(e, r);
		});
	}
	else if(par.topics && par.topics.length > 0) update_news_topic(par.id, par.topics, fn);
}

exports.delete = function(par, fn){
	db.query("DELETE FROM news WHERE id=?", par, fn);
}
