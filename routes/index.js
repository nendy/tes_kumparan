const express = require('express');
const fs = require("fs");
const router = express.Router();

/*	setup /api routing
	process all files in route folder witch contain api_ prefix
*/
fs.readdir(__dirname, (err, list) => {
	if (err)console.log(err);
	list.forEach(file => {
		const api = file.match("api_(.*).js");
		if(api){
			require('./'+api[0])(router);
			console.log('load modul API:',api[1]);
		}
	});
});

module.exports = app => {
	app.get('/', (req, res) => {
		res.render('index');
	});
	app.get('/news', (req, res) => {
		res.render('news');
	});
	app.get('/topics', (req, res) => {
		res.render('topics');
	});
	app.get('/template/:mod', (req, res) => {
		res.render('template/'+req.params.mod);
	});
	app.use('/api', (req, res, next) => {
		req.log_stamp();
		next();
	}, router);
};