const model = require('../model/news');

module.exports = router => {

	router.route('/news/:id').get((req, res) => {
		req.checkParams('id', 'Id is integer').isInt();
		const errors = req.validationErrors();
		if (errors) return res.status(400).json({stat:2, mess:'There have been validation errors', info: errors});

		model.detail(req.params.id, (e, r) => {
			if(e) return res.with(500, e);
			else res.json({stat:1, data: r[0]});
		});
	}).put((req, res) => {
		req.checkParams('id', 'id is integer').notEmpty().isInt();

		const errors = req.validationErrors();
		if (errors) return res.status(400).json({stat:2, mess:'There have been validation errors', info: errors});
		let par = {
			update: {},
			id: req.params.id
		};
		if(req.body.title) par.update.title = req.body.title;
		if(req.body.content) par.update.content = req.body.content;
		if(req.body.topics) par.topics = req.body.topics;
		console.log('params:', par);

		if(Object.keys(par.update).length == 0 && !par.topics) return res.with(2, 'noting to update');
		model.update(par, function(e, r){
			if(e) return res.with(500, e);
			res.json({stat:1, mess: 'Updating OK'});
		})
	}).delete(function(req,res){
		req.checkParams('id', 'id is integer').notEmpty().isInt();

		var errors = req.validationErrors();
		if (errors) return res.status(400).json({stat:2,mess:'There have been validation errors', info:errors});
		model.delete(req.params.id, function(e, r){
			if(e) return res.with(500,e);
			res.json({stat:1,msg:'Deleting OK'});
		});
	});
	router.route('/news').get((req, res) => {
		//input validation
		req.checkQuery('page', 'page is integer').optional().isInt();
		req.checkQuery('limit', 'limit is integer').optional().isInt();
		//sanitize input
		req.sanitizeQuery('start').toInt();
		req.sanitizeQuery('limit').toInt();
		const errors = req.validationErrors();
		if (errors) return res.json({stat:2,mess:'There have been validation errors', info: errors});
		let par = {};
		par.start = req.query.start || 0;
		par.limit = req.query.limit || 15;

		console.log('list query:',req.query);

		model.list(par, (e, r) => {
			if(e) return res.with(500,e);
			res.json(r);
		});
	})
	.post((req, res) => {
		req.checkBody('title', 'title is mandatary').notEmpty();
		req.checkBody('content', 'content is mandatary').notEmpty();
		const errors = req.validationErrors();
		if (errors) return res.json({stat: 2, mess:'There have been validation errors', info: errors});

		let params = {
			title: req.body.title,
			content: req.body.content
		}
		model.add(params, (e, r) => {
			if(e) return res.with(500, e);
			res.json({stat: 1, new_id: r.insertId});
		});
	});
}