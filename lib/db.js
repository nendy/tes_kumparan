const mysql = require('mysql');
const CFG = require('../config')
const QUERY_LOG = process.env.QUERY_LOG;
const mysql_pool = mysql.createPool({
	connectionLimit : 100,
	host : CFG.db_host,
	database : CFG.db_name,
	user : CFG.db_user,
	password : CFG.db_pass,
	dateStrings:true
});

module.exports.query = function() {
	const arg = arguments;
	mysql_pool.getConnection((e, conn) => {
		if(e) console.log('mysql error:', e);
		else {
			const act = conn.query.apply(conn, arg);
			if(QUERY_LOG == 1) console.log('do_query:', act.sql);
			conn.release();
		}
	});
};
